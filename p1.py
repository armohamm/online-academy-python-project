# nama file p1.py 
# Isikan email anda dan copy semua cell code yang dengan komentar #Graded
# Isikan juga priority file

# untuk resubmisi, grader hanya akan mengambil priority yang paling besar
# jadi kalau anda ingin merevisi kode anda:
# silakan di resubmit dengan nilai variable priority yang lebih besar dari
# nilai priority submisi sebelumnya
# JIKA TIDAK ADA VARIABLE priority DIANGGAP priority=0

priority = 2


#netacad email cth: 'abcd@gmail.com'
email='pristinashop@gmail.com'

# copy-paste semua #Graded cells di bawah ini

# PASTE KODE ANDA DISINI 
#Graded

# Manual, filter, list comprehension
def letter_catalog(items,letter='A'):
  pass
  res = []
  for f in items:
      if(f[0] == letter):
          res.append(f)
  return res

print(letter_catalog(['Apple','Avocado','Banana','Blackberries','Blueberries','Cherries'],letter='A'))
#Graded

def counter_item(items):
  pass
  items.sort()
  nTemp = items[0]
  bEqual = True
  d= {}
  d[items[0]] = items.count(items[0])
  for item in items:
      if nTemp != item:
          d[item] = items.count(item)
  return d

print(counter_item(['Apple','Apple','Apple','Blueberries','Blueberries','Blueberries']))

#Graded

# dua variable berikut jangan diubah
fruits = ['Apple','Avocado','Banana','Blackberries','Blueberries','Cherries','Date Fruit','Grapes','Guava','Jackfruit','Kiwifruit']
prices = [6,5,3,10,12,7,14,15,8,7,9]

# list buah
chart = ['Blueberries','Blueberries','Grapes','Apple','Apple','Apple','Blueberries','Guava','Jackfruit','Blueberries','Jackfruit']

# MULAI KODEMU DI SINI
fruit_price = dict(zip(fruits, prices))

def total_price(dcounter,fprice):
  pass
  total = 0
  for key in fprice:
      if (key in fprice)and(key in dcounter):
          key = (fprice[key] * dcounter[key])
          total = key + total
  return total

print(total_price(counter_item(chart),fruit_price))

#Graded

def discounted_price(total,discount,minprice=100):
  pass
  if total >= minprice:
      discount = discount/100
  else:
      discount = 0
  total_discount = total*discount
  return total-total_discount
    
print(discounted_price(total_price(counter_item(chart),fruit_price),10,minprice=100))


#Graded

def print_summary(items,fprice):
  pass
  dcounter = counter_item(items)
  for key, value in fprice.items():
    if (key in fprice)and(key in dcounter):
     print (dcounter[key], key, ":", dcounter[key]*value)
  print("total :",total_price(dcounter,fprice))
  print("discount price :", discounted_price(total_price(dcounter,fprice),10,minprice=100))
print_summary(chart,fruit_price)
